import * as ex from 'excalibur'
import { Resources, Loader } from './resources'
import { asg } from './utils'

const game = new ex.Engine({
  backgroundColor: ex.Color.Black,
  displayMode: ex.DisplayMode.FullScreen
});

const player = new ex.Actor(0, 0, 22, 22)
player.addDrawing(Resources.Bubble);

game.add(player)

game.input.pointers.primary.on('move', (evt) => {
  const {x, y} = evt.target.lastWorldPos
  asg(player.pos, {x, y})
})

game.start(Loader)
