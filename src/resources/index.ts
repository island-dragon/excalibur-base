import * as ex from 'excalibur';
const bubble = require('./images/bubble.png');

let Resources = {
    Bubble: new ex.Texture(bubble)
}

let Loader = new ex.Loader();
Loader.addResources(Object.values(Resources))

export { Resources, Loader }
